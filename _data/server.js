const path = require('path');
const jsonServer = require('json-server');

const server = jsonServer.create();
const router = jsonServer.router(path.join(__dirname, 'data.json'));
const routes = require(path.join(__dirname, 'routes.json'));

// middlewares
server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);

// users end-point
const users = [
  {
    id: 1,
    email: 'test@test.com',
    name: '테스트계정',
    password: '1234',
  },
];

server.get('/users/me', (req, res) => {
  const accessToken = req.headers.authorization?.split(`Bearer `)[1];
  if (!accessToken) {
    return res.status(400).json({
      statusCode: 400,
      name: 'GlobalException',
      message: 'invalid access token.',
    });
  }

  const user = users.find((each) => each.id === accessToken);

  return res.json({ ...user, password: undefined });
});

server.post('/users/sign-up', (req, res) => {
  const { name, email, password } = req.body;

  if (users.find((each) => each.email === email)) {
    return res.status(400).json({
      statusCode: 400,
      name: 'SignUpException',
      message: 'already email exist.',
    });
  }

  const id = Date.now();
  users.push({ id, name, email, password });

  return res.status(201).json({ id });
});

server.post('/users/sign-in', (req, res) => {
  const { email, password } = req.body;
  const user = users.find((each) => each.email === email && each.password === password);

  if (!user) {
    return res.status(400).json({
      statusCode: 400,
      name: 'SignInUserException',
      message: 'invalid email or password.',
    });
  }

  return res.status(201).json({
    accessToken: user.id.toString(),
    refreshToken: user.id.toString() + '-R',
  });
});

// server on
const PORT = 4000;

server.use(jsonServer.rewriter(routes));
server.use(router);

server.listen(PORT, () => console.log(`JSON Server is running on port ${PORT}`));
