/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'an2-img.amz.wtchn.net',
      },
    ],
  },
  experimental: {
    serverComponentsExternalPackages: ['import2'],
  },
  // logging: {
  //   fetches: {
  //     fullUrl: true,
  //   },
  // },
};

module.exports = nextConfig;
