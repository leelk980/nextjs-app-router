import { cache } from 'react';

import { dehydrate, HydrationBoundary, QueryClient } from '@tanstack/react-query';

const getQueryClient = cache(() => new QueryClient());

/**
 * ! this is only for page.tsx
 */
export const HydrationProvider = async (props: {
  children: React.ReactNode;
  fns: ((queryClient: QueryClient) => Promise<void>)[];
}) => {
  const queryClient = getQueryClient();

  await Promise.all(props.fns.map((each) => each(queryClient)));

  const state = dehydrate(queryClient);

  return <HydrationBoundary state={state}>{props.children}</HydrationBoundary>;
};
