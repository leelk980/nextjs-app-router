import {
  Modal as ChakraModal,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  ModalProps,
} from '@chakra-ui/react';

type Props = ModalProps;

export const Modal = (props: Props) => {
  return (
    <ChakraModal {...props}>
      <ModalOverlay />

      <ModalContent>
        <ModalHeader>
          <ModalCloseButton />
        </ModalHeader>

        {props.children}
      </ModalContent>
    </ChakraModal>
  );
};
