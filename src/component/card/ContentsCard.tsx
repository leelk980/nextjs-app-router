'use client';
import Image from 'next/image';
import Link from 'next/link';
import { useMemo } from 'react';

import { useGlobalStore } from '@/hook';
import { Text, VStack } from '@chakra-ui/react';

type Props = {
  movie: {
    id: number;
    badge: string;
    thumbnailUrl: string;
    title: string;
    year: string;
    country: string;
  };
  flex?: string;
};

export const ContentsCard = (props: Props) => {
  const { isDesktop } = useGlobalStore();

  const imageSize = useMemo(
    () => ({
      width: isDesktop ? 200 : 160,
      height: isDesktop ? 320 : 256,
    }),
    [isDesktop]
  );

  return (
    <VStack alignItems={'flex-start'} flex={props.flex || ''}>
      <Text
        px={2}
        py={1}
        borderRadius={4}
        bgColor={'gray.800'}
        opacity={0.8}
        color={'white'}
        as={'b'}
        ml={2}
        mt={2}
        mb={-12}
      >
        {props.movie.badge}
      </Text>

      <Link href={`/contents/${props.movie.id}`}>
        <Image src={props.movie.thumbnailUrl} alt="image" {...imageSize} />
      </Link>

      <VStack alignItems={'flex-start'} spacing={0}>
        <Text as="b">{props.movie.title}</Text>
        <Text fontSize={'sm'}>
          {props.movie.year} ・ {props.movie.country}
        </Text>
      </VStack>
    </VStack>
  );
};
