import { Grid, GridItem, HStack, VStack } from '@chakra-ui/react';

export const VContainer = VStack;

export const HContainer = HStack;

export const GContainer = Grid;

export const GItem = GridItem;
