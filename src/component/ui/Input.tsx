import { debounceLast } from '@/util';
import {
  Input as ChakraInput,
  InputGroup,
  InputLeftElement,
  InputProps,
  InputRightElement,
  Text,
  VStack,
} from '@chakra-ui/react';

type Props = InputProps & {
  leftElement?: React.ReactNode;
  rightElement?: React.ReactNode;
  errorMessage?: string;
  debounceTime?: number;
};

export const Input = (props: Props) => {
  const { leftElement, rightElement, errorMessage, debounceTime, onChange, ...origin } = props;

  if (!origin.size) {
    origin.size = 'lg';
  }
  if (!origin.bgColor) {
    origin.bgColor = 'gray.100';
  }
  if (!origin.opacity) {
    origin.opacity = 0.4;
  }
  if (!origin.focusBorderColor) {
    origin.focusBorderColor = 'gray.400';
  }

  return (
    <VStack width={'100%'} alignItems={'flex-start'} spacing={4}>
      <InputGroup>
        {leftElement && (
          <InputLeftElement height={'100%'} pointerEvents="none">
            {leftElement}
          </InputLeftElement>
        )}

        <ChakraInput
          {...origin}
          onChange={
            onChange && debounceTime !== 0 ? debounceLast(onChange, debounceTime ?? 500) : onChange
          }
        />

        {rightElement && (
          <InputRightElement height={'100%'} pointerEvents="none">
            {rightElement}
          </InputRightElement>
        )}
      </InputGroup>

      {errorMessage && (
        <Text
          size={'md'}
          alignSelf={'flex-start'}
          textColor={'red'}
          px={2}
        >{`* ${errorMessage}`}</Text>
      )}
    </VStack>
  );
};
