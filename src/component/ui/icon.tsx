'use client';
import { InfoIcon, SearchIcon, SunIcon } from '@chakra-ui/icons';

export const Icon = {
  Info: InfoIcon,
  Search: SearchIcon,
  Sun: SunIcon,
};
