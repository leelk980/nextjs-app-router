import { Heading, HeadingProps, HStack } from '@chakra-ui/react';

type Props = HeadingProps;

export const Logo = (props: Props) => {
  return (
    <HStack spacing={1}>
      <Heading {...props} color={'pink.400'}>
        WATCHA
      </Heading>
      <Heading {...props} fontWeight={'200'}>
        PEDIA
      </Heading>
    </HStack>
  );
};
