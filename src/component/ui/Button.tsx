import { debounce } from '@/util';
import { Button as ChakraButton, ButtonProps } from '@chakra-ui/react';

type Props = ButtonProps & {
  debounceTime?: number;
};

export const Button = (props: Props) => {
  const { debounceTime, onClick, ...origin } = props;

  return (
    <ChakraButton
      {...origin}
      onClick={onClick && debounceTime !== 0 ? debounce(onClick, debounceTime ?? 1000) : onClick}
    />
  );
};
