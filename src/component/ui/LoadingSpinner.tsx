import { Button } from '@chakra-ui/react';

export const LoadingSpinner = () => (
  <Button
    mx={'auto'}
    my={2}
    isLoading
    fontSize={32}
    loadingText={''}
    variant={'link'}
    spinnerPlacement={'start'}
  />
);
