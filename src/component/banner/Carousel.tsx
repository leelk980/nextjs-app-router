'use client';

import 'react-responsive-carousel/lib/styles/carousel.min.css';

import Image from 'next/image';
import { Carousel as C } from 'react-responsive-carousel';

type Props = {
  width: number;
  height: number;
  images: {
    src: string;
    alt: string;
  }[];
};

export const Carousel = (props: Props) => {
  return (
    <C
      showThumbs={false}
      showArrows={false}
      showStatus={false}
      autoPlay={true}
      infiniteLoop={true}
      interval={6000}
    >
      {props.images.map((each, index) => (
        <Image
          key={index}
          src={each.src}
          alt={each.alt}
          width={props.width}
          height={props.height}
        />
      ))}
    </C>
  );
};
