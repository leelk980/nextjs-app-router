'use server';
import { cookies } from 'next/headers';

import { functional, IConnection } from '@/sdk/api/module';
import { QueryClient } from '@tanstack/react-query';

import { connection, ParametersExcludingFirst } from './_types';
import { CookieStorageKey } from './useStorage';

export const useApiPrefetchQuery = <T extends (conn: IConnection, ...args: any[]) => Promise<any>>(
  fn: (params: typeof functional) => T
) => {
  return {
    data<Body extends ParametersExcludingFirst<T>>(...body: Body) {
      if (typeof window !== 'undefined') {
        connection.headers.authorization = ``;
      } else {
        const cookieStore = cookies();
        const accessToken = cookieStore.get('accessToken' satisfies CookieStorageKey)?.value;

        connection.headers.authorization = accessToken ? `Bearer ${accessToken}` : ``;
      }

      const target = fn(functional);

      const { method, path } = (target as any).METADATA;

      return (queryClient: QueryClient) =>
        queryClient.prefetchQuery({
          queryKey: [method, path, ...body],
          queryFn: () => target(connection, ...body),
        });
    },
  };
};
