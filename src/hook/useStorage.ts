'use client';
import { useCookies } from 'react-cookie';

export type CookieStorageKey = 'accessToken';

export type LocalStorageKey = '';

export const useStorage = (
  params:
    | {
        type: 'cookie';
        key: CookieStorageKey;
      }
    | {
        type: 'local';
        key: LocalStorageKey;
      }
) => {
  if (params.type === 'cookie') {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [cookie, setCookie, removeCookie] = useCookies([params.key]);

    return [
      (cookie[params.key] || null) as string | null,
      (value: string, options?: Parameters<typeof setCookie>[2]) =>
        setCookie(params.key, value, options),
      (options?: Parameters<typeof setCookie>[2]) => removeCookie(params.key, options),
    ] as const;
  }

  return [
    window.localStorage.getItem(params.key),
    (value: string) => window.localStorage.setItem(params.key, value),
    () => window.localStorage.removeItem(params.key),
  ] as const;
};
