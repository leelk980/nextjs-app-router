'use client';
import { GlobalException } from '@/sdk/@libs/common';
import { functional, IConnection } from '@/sdk/api/module';
import { useMutation } from '@tanstack/react-query';

import { useStorage } from './';
import { connection, ParametersExcludingFirst } from './_types';

export const useApiMutation = <T extends (conn: IConnection, ...args: any[]) => Promise<any>>(
  fn: (params: typeof functional) => T
) => {
  return {
    data<Body extends ParametersExcludingFirst<T>, Response extends Awaited<ReturnType<T>>>(
      ...body: Body
    ) {
      if (typeof window !== 'undefined') {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [accessToken] = useStorage({ type: 'cookie', key: 'accessToken' });

        connection.headers.authorization = accessToken ? `Bearer ${accessToken}` : ``;
      } else {
        connection.headers.authorization = ``;
      }

      const target = fn(functional);

      const { method, path } = (target as any).METADATA;

      // eslint-disable-next-line react-hooks/rules-of-hooks
      const { mutateAsync, isPending } = useMutation<Response>({
        mutationKey: [method, path, ...body],
        mutationFn: () => target(connection, ...body),
      });

      return async () => {
        const res = await mutateAsync();

        if (!res) {
          if (isPending) {
            return { data: null, error: null, isPending };
          }

          return {
            data: null,
            error: {
              name: 'GlobalException',
              statusCode: 500,
              message: 'internal server error.',
            } as GlobalException,
            isPending,
          };
        }

        if (res.status < 400) {
          return {
            data: res.data as Exclude<Response, { status: 400 | 500 }>['data'],
            error: null,
            isPending,
          };
        }

        return {
          data: null,
          // prettier-ignore
          error: res.data as Exclude<Response, { status: 200 | 201 | 202 | 203 | 204 }>['data'] | GlobalException,
          isPending,
        };
      };
    },
  };
};
