'use client';
import { functional, IConnection } from '@/sdk/api/module';
import { useQueryClient } from '@tanstack/react-query';

import { ParametersExcludingFirst } from './_types';

export const useApiInvalidation = <T extends (conn: IConnection, ...args: any[]) => Promise<any>>(
  fn: (params: typeof functional) => T
) => {
  return {
    data<Body extends ParametersExcludingFirst<T>>(...body: Body) {
      const target = fn(functional);

      const { method, path } = (target as any).METADATA;

      // eslint-disable-next-line react-hooks/rules-of-hooks
      const queryClient = useQueryClient();

      return () => {
        setTimeout(
          () =>
            queryClient.invalidateQueries({
              queryKey: [method, path, ...body],
            }),
          0
        );
      };
    },
  };
};
