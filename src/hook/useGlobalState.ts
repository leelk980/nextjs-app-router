'use client';
import { create } from 'zustand';

type GlobalState = {
  isDesktop: boolean;

  setGlobalState: (state: Partial<Omit<GlobalState, 'setGlobalState'>>) => void;
};

export const useGlobalStore = create<GlobalState>((set) => {
  return {
    isDesktop: global ? true : window.innerWidth >= 600,

    setGlobalState: (state) => set((prev) => ({ ...prev, ...state })),
  };
});
