'use client';
import { GlobalException } from '@/sdk/@libs/common';
import { functional, IConnection } from '@/sdk/api/module';
import { useQuery } from '@tanstack/react-query';

import { useStorage } from './';
import { connection, ParametersExcludingFirst } from './_types';

export const useApiQuery = <T extends (conn: IConnection, ...args: any[]) => Promise<any>>(
  fn: (params: typeof functional) => T
) => {
  return {
    data<Body extends ParametersExcludingFirst<T>, Response extends Awaited<ReturnType<T>>>(
      ...body: Body
    ) {
      if (typeof window !== 'undefined') {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [accessToken] = useStorage({ type: 'cookie', key: 'accessToken' });

        connection.headers.authorization = accessToken ? `Bearer ${accessToken}` : ``;
      } else {
        connection.headers.authorization = ``;
      }

      const target = fn(functional);

      const { method, path } = (target as any).METADATA;

      // eslint-disable-next-line react-hooks/rules-of-hooks
      const { data, isLoading } = useQuery({
        queryKey: [method, path, ...body],
        queryFn: () => target(connection, ...body),
      });

      if (!data) {
        if (isLoading) {
          console.log('loading');

          return { data: null, error: null, isLoading };
        }

        return {
          data: null,
          error: {
            name: 'GlobalException',
            statusCode: 500,
            message: 'internal server error.',
          } as GlobalException,
          isLoading,
        };
      }

      if (data.status < 400) {
        return {
          data: data.data as Exclude<Response, { status: 400 | 500 }>['data'],
          error: null,
          isLoading,
        };
      }

      return {
        data: null,
        // prettier-ignore
        error: data.data as Exclude<Response, { status: 200 | 201 | 202 | 203 | 204 }>['data'] | GlobalException,
        isLoading,
      };
    },
  };
};
