import { IConnection } from '@nestia/fetcher';

export const connection = {
  host: 'http://localhost:4000',
  headers: {
    authorization: ``,
  },
} satisfies IConnection;

export type ParametersExcludingFirst<T extends (...args: any) => any> = T extends (
  first: any,
  ...rest: infer U
) => any
  ? U
  : never;
