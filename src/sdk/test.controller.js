"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MovieWatchaRankException = exports.MovieUpcommingReleaseException = exports.MovieBoxOfficeRankException = exports.TestController = void 0;
const common_1 = require("./@libs/common");
const auth_guard_1 = require("./@libs/middleware/auth.guard");
const core_1 = require("@nestia/core");
const common_2 = require("@nestjs/common");
let TestController = class TestController {
    async movieBoxOfficeRank() {
        return new Array(10).fill(0).map(() => ({
            id: 1,
            rank: 1,
            title: '윙카',
            year: '2023',
            country: '미국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/Vnn_KYM-QxlK00ydZiSpKA.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFM01ESXdNakl6TURJek1qQXdOVGs1TnpFaWZRLnZXakM5cEZMT2hzLXhnOXl3dGpsdU1RVHpZSUw2NFRBazRsZ3RiUFdhdXc',
        }));
    }
    async movieUpcommingRelease() {
        return new Array(10).fill(0).map(() => ({
            id: 2,
            rank: 1,
            title: '톡투미',
            year: '2024',
            country: '미국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/5Ml7be_uJ_ybHvmwPhhx-Q.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFM01EQXhPREUwTlRBeU9Ua3pNamcxTVRVaWZRLmZ0SUJPelV0VldRU1hWelJQSFg5Mml0TU8tcF9BUGQ3LUgtSnFrTE5STGs',
        }));
    }
    async movieWatchaRank() {
        return new Array(10).fill(0).map(() => ({
            id: 3,
            rank: 1,
            title: '서울의 봄',
            year: '2023',
            country: '한국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/y8zw23wQG88i2Y3lNWetpQ.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFMk9UazFPVEkwTmpnM016QTVNamd6TWpFaWZRLjFQU194eWZtVWFUZG5KUmhsY2V5RHVlVnZXVVhEQ2hhYlhnY01KZ1Fka1k',
        }));
    }
    async movieWatchaRankWithQuery(query) {
        query;
        return new Array(10).fill(0).map(() => ({
            id: 3,
            rank: 1,
            title: '서울의 봄',
            year: '2023',
            country: '한국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/y8zw23wQG88i2Y3lNWetpQ.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFMk9UazFPVEkwTmpnM016QTVNamd6TWpFaWZRLjFQU194eWZtVWFUZG5KUmhsY2V5RHVlVnZXVVhEQ2hhYlhnY01KZ1Fka1k',
        }));
    }
};
exports.TestController = TestController;
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "MovieBoxOfficeRankException"),
    core_1.TypedRoute.Get('/movieBoxOfficeRank', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TestController.prototype, "movieBoxOfficeRank", null);
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "MovieUpcommingReleaseException"),
    core_1.TypedRoute.Get('/movieUpcommingRelease', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TestController.prototype, "movieUpcommingRelease", null);
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "MovieWatchaRankException"),
    core_1.TypedRoute.Get('/movieWatchaRank', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TestController.prototype, "movieWatchaRank", null);
__decorate([
    (0, core_1.TypedException)(400, undefined, "MovieWatchaRankException"),
    core_1.TypedRoute.Get('/movieWatchaRankWithQuery', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "Movie",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<Movie>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __param(0, (0, core_1.TypedQuery)({ type: "assert", assert: input => { const decode = input => {
            const $params = core_1.TypedQuery.params;
            const $number = core_1.TypedQuery.number;
            const $string = core_1.TypedQuery.string;
            input = $params(input);
            const output = {
                id: $number(input.get("id")),
                rank: $number(input.get("rank")),
                title: $string(input.get("title")),
                year: $string(input.get("year")),
                country: $string(input.get("country")),
                thumbnailUrl: $string(input.get("thumbnailUrl"))
            };
            return output;
        }; const assert = input => {
            const __is = input => {
                return "object" === typeof input && null !== input && ("number" === typeof input.id && "number" === typeof input.rank && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl);
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedQuery.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return ("object" === typeof input && null !== input || $guard(true, {
                        path: _path + "",
                        expected: "Movie",
                        value: input
                    })) && $ao0(input, _path + "", true) || $guard(true, {
                        path: _path + "",
                        expected: "Movie",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const output = decode(input); return assert(output); } })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TestController.prototype, "movieWatchaRankWithQuery", null);
exports.TestController = TestController = __decorate([
    (0, common_2.Controller)('/movies')
], TestController);
class MovieBoxOfficeRankException extends common_1.AppException {
}
exports.MovieBoxOfficeRankException = MovieBoxOfficeRankException;
class MovieUpcommingReleaseException extends common_1.AppException {
}
exports.MovieUpcommingReleaseException = MovieUpcommingReleaseException;
class MovieWatchaRankException extends common_1.AppException {
}
exports.MovieWatchaRankException = MovieWatchaRankException;
//# sourceMappingURL=test.controller.js.map