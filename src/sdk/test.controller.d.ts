import { AppException } from '@libs/common';
export declare class TestController {
    movieBoxOfficeRank(): Promise<Movie[]>;
    movieUpcommingRelease(): Promise<Movie[]>;
    movieWatchaRank(): Promise<Movie[]>;
    movieWatchaRankWithQuery(query: Movie): Promise<Movie[]>;
}
export interface Movie {
    id: number;
    rank: number;
    title: string;
    year: string;
    country: string;
    thumbnailUrl: string;
}
export declare class MovieBoxOfficeRankException extends AppException<'notfound movieBoxOfficeRank.'> {
}
export declare class MovieUpcommingReleaseException extends AppException<'notfound movieUpcommingRelease.'> {
}
export declare class MovieWatchaRankException extends AppException<'notfound movieWatchaRank.'> {
}
