"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: `configs/dotenv/.env.${process.env['NODE_ENV'] || 'local'}` });
/* eslint-disable @typescript-eslint/no-non-null-assertion */
exports.environment = {
    tz: process.env.TZ,
    node: {
        env: process.env['NODE_ENV'],
    },
    database: {
        host: process.env['DATABASE_HOST'],
        port: +process.env['DATABASE_PORT'],
        username: process.env['DATABASE_USERNAME'],
        password: process.env['DATABASE_PASSWORD'],
        name: process.env['DATABASE_NAME'],
    },
    cache: {
        host: process.env['CACHE_HOST'],
        port: +process.env['CACHE_PORT'],
    },
    jwt: {
        access: {
            secret: process.env['JWT_ACCESS_SECRET'],
            expire: process.env['JWT_ACCESS_EXPIRE'],
        },
        refresh: {
            secret: process.env['JWT_REFRESH_SECRET'],
            expire: process.env['JWT_REFRESH_EXPIRE'],
        },
    },
    aws: {
        access: process.env['AWS_ACCESS'],
        secret: process.env['AWS_SECRET'],
        s3: {
            bucket: process.env['AWS_S3_BUCKET'],
        },
    },
};
//# sourceMappingURL=environment.js.map