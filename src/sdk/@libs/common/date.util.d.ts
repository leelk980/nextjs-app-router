type Datable = Date | string;
export declare class DateUtil {
    static addMinute(target: Datable, minute: number): Date;
    static minusMinute(target: Datable, minute: number): Date;
    static isBefore(start: Datable, end: Datable): boolean;
    static isAfter(end: Datable, start: Datable): boolean;
    static toDateString(date: Date): string;
    static toKrDateString(date: Date): string;
}
export {};
