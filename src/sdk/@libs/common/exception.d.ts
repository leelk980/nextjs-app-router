export declare abstract class AppException<T extends string> extends Error {
    readonly name: string;
    readonly message: T;
    readonly statusCode: number;
    /** @hidden */
    readonly stack: string;
    /** @hidden */
    readonly additional: Record<string, any> | undefined;
    constructor(message: T, statusCode?: number, additional?: Record<string, any>);
}
export declare class GlobalException extends AppException<'invalid access token.' | 'invalid request data.' | 'internal server error.'> {
}
