"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateUtil = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
class DateUtil {
    static addMinute(target, minute) {
        return (0, dayjs_1.default)(target).add(minute, 'minute').toDate();
    }
    static minusMinute(target, minute) {
        return (0, dayjs_1.default)(target).subtract(minute, 'minute').toDate();
    }
    static isBefore(start, end) {
        return (0, dayjs_1.default)(start).isBefore(end) || (0, dayjs_1.default)(start).isSame(end);
    }
    static isAfter(end, start) {
        return (0, dayjs_1.default)(start).isBefore(end) || (0, dayjs_1.default)(start).isSame(end);
    }
    static toDateString(date) {
        const dateString = date.toISOString().split('T')[0];
        if (!dateString) {
            throw new Error(`cannot convert to date string. ${date}`);
        }
        return dateString;
    }
    static toKrDateString(date) {
        return (0, dayjs_1.default)(date).format('YYYY년 M월 D일');
    }
}
exports.DateUtil = DateUtil;
//# sourceMappingURL=date.util.js.map