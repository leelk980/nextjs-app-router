import typia from 'typia';
import { indexBy, pipe } from '@fxts/core';
export declare class F {
    static pipe: typeof pipe;
    static assoicateBy: typeof indexBy;
    /**
     * 객체에서 `undefined` 값 속성을 제거합니다.
     * @param target `undefined` 속성을 제거할 객체입니다.
     * @return `undefined` 속성이 제거된 객체입니다.
     */
    static removeUndefined<T extends Record<string, any>>(target: T): T;
    static groupBy<T extends Record<string, any>, U extends string>(fn: (each: T) => U, keys?: U[]): (records: T[]) => Record<U, T[]>;
    static mapValues<T extends string, U, V>(fn: (entry: {
        key: T;
        value: U;
    }) => V): (record: Record<T, U>) => import("@fxts/core/dist/types/types/ReturnPipeType").default<[Generator<T extends string ? [T, Record<T, U>[T]] : never, void, unknown>, IterableIterator<[T, V]>, import("@fxts/core/dist/types/types/Awaited").default<{ [K in [T, V] as K[0]]: K[1]; }>], import("@fxts/core/dist/types/types/Awaited").default<import("@fxts/core/dist/types/types/Awaited").default<import("@fxts/core/dist/types/types/Awaited").default<{ [K in [T, V] as K[0]]: K[1]; }>> extends never ? never : import("@fxts/core/dist/types/types/Awaited").default<{ [K in [T, V] as K[0]]: K[1]; }>>>;
    static toUnique<T>(arr: T[]): T[];
    static uniqueBy<T>(arr: T[], fn: (each: T) => string): T[];
    static validatePrune: typeof typia.misc.validatePrune;
}
