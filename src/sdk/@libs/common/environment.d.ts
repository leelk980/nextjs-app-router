export declare const environment: {
    tz: string;
    node: {
        env: string;
    };
    database: {
        host: string;
        port: number;
        username: string;
        password: string;
        name: string;
    };
    cache: {
        host: string;
        port: number;
    };
    jwt: {
        access: {
            secret: string;
            expire: string;
        };
        refresh: {
            secret: string;
            expire: string;
        };
    };
    aws: {
        access: string;
        secret: string;
        s3: {
            bucket: string;
        };
    };
};
