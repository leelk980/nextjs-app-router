import { UserRoleEnum } from '@libs/common';
import { ClsManager, JwtManager } from '@libs/infrastructure/manager';
import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
declare const AuthSymbol: unique symbol;
type AuthType = UserRoleEnum | 'public';
export declare const Auth: (...auths: AuthType[]) => import("@nestjs/common").CustomDecorator<typeof AuthSymbol>;
export declare class AuthGuard implements CanActivate {
    private readonly reflector;
    private readonly jwtManager;
    private readonly clsManager;
    constructor(reflector: Reflector, jwtManager: JwtManager, clsManager: ClsManager);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export {};
