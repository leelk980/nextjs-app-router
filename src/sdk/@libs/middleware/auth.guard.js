"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthGuard = exports.Auth = void 0;
const common_1 = require("../common");
const manager_1 = require("../infrastructure/manager");
const common_2 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const AuthSymbol = Symbol('auth');
const Auth = (...auths) => (0, common_2.SetMetadata)(AuthSymbol, auths);
exports.Auth = Auth;
let AuthGuard = class AuthGuard {
    constructor(reflector, jwtManager, clsManager) {
        this.reflector = reflector;
        this.jwtManager = jwtManager;
        this.clsManager = clsManager;
    }
    async canActivate(context) {
        var _a, _b, _c, _d;
        const req = context.switchToHttp().getRequest();
        const auths = this.reflector.getAllAndOverride(AuthSymbol, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (auths.includes('public')) {
            const accessToken = (_b = (_a = req === null || req === void 0 ? void 0 : req.headers) === null || _a === void 0 ? void 0 : _a.authorization) === null || _b === void 0 ? void 0 : _b.split('Bearer ')[1];
            if (accessToken !== '123') {
                console.log(false);
                return false;
            }
            console.log(true);
            return true;
        }
        const accessToken = (_d = (_c = req === null || req === void 0 ? void 0 : req.headers) === null || _c === void 0 ? void 0 : _c.authorization) === null || _d === void 0 ? void 0 : _d.split('Bearer ')[1];
        if (!accessToken) {
            throw new common_1.GlobalException('invalid access token.');
        }
        const payload = this.jwtManager.verify({ type: 'access', token: accessToken });
        if (!payload) {
            throw new common_1.GlobalException('invalid access token.');
        }
        this.clsManager.setItem('currUser', {
            id: payload.userId,
            companyId: payload.companyId,
        });
        return true;
    }
};
exports.AuthGuard = AuthGuard;
exports.AuthGuard = AuthGuard = __decorate([
    (0, common_2.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector,
        manager_1.JwtManager,
        manager_1.ClsManager])
], AuthGuard);
//# sourceMappingURL=auth.guard.js.map