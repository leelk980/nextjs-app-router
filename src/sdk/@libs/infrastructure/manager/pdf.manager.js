"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PdfManager = void 0;
const date_util_1 = require("../../common/date.util");
const common_1 = require("@nestjs/common");
const pdfmake_1 = __importDefault(require("pdfmake"));
let PdfManager = class PdfManager {
    constructor() {
        this.printer = new pdfmake_1.default({
            Spoqa: {
                normal: 'static/fonts/SpoqaHanSansNeo-Regular.ttf',
                bold: 'static/fonts/SpoqaHanSansNeo-Bold.ttf',
                italics: 'static/fonts/SpoqaHanSansNeo-Medium.ttf',
                bolditalics: 'static/fonts/SpoqaHanSansNeo-Medium.ttf',
            },
        });
    }
    generatePdfStream(type, params) {
        let pdfDoc = null;
        if (type === 'companySpecification') {
            pdfDoc = this.printer.createPdfKitDocument({
                content: [
                    // Header
                    {
                        alignment: 'justify',
                        stack: [
                            {
                                columns: [
                                    {
                                        image: 'logo',
                                        width: 100,
                                        height: 28,
                                        marginLeft: 16,
                                    },
                                    {
                                        alignment: 'center',
                                        width: '58%',
                                        bold: true,
                                        stack: [
                                            {
                                                text: '(주) 리뉴어스랩',
                                                fontSize: 10,
                                            },
                                            {
                                                text: '온실가스 배출량에 대한 명세서',
                                                fontSize: 12,
                                            },
                                        ],
                                    },
                                    {
                                        style: { color: '#575757' },
                                        fontSize: 8,
                                        stack: [
                                            {
                                                text: `문서번호 : ${params.docId}`,
                                            },
                                            {
                                                text: `발송일자 : ${date_util_1.DateUtil.toKrDateString(params.publishedAt)}`,
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0,
                                        y1: 4,
                                        x2: 520,
                                        y2: 4,
                                        lineWidth: 1.5,
                                        lineColor: '#575757',
                                    },
                                ],
                            },
                        ],
                    },
                    // Main
                    {
                        marginTop: 28,
                        stack: [
                            {
                                text: '가. 서문',
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `(주)리뉴어스랩(이하 당사)은 ${params.company.name}의 국내 ${params.company.companyFacilities.length}개 사업장(이하 ${params.company.name})의
                ${date_util_1.DateUtil.toKrDateString(params.company.startDate)} ~ ${date_util_1.DateUtil.toKrDateString(params.company.endDate)} 온실가스 배출량(Scope 1, 2)에 대한 산정을 수행했습니다.
                국내 사업장: ${params.company.companyFacilities
                                    .map((each) => `${each.name} (${each.address})`)
                                    .join(', ')}`,
                                marginBottom: 20,
                            },
                            {
                                text: `나. 산정 범위`,
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `${params.company.name}의 운영 통제 하에 있는 ${params.company.companyFacilities
                                    .map((each) => each.name)
                                    .join(', ')} Scope 1, 2 온실가스 배출원을 대상으로 했습니다.`,
                                marginBottom: 20,
                            },
                            {
                                text: `다. 산정 기준`,
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `“온실가스 배출권 거래제의 배출량 보고 및 인증에 관한 지침(환경부고시 제2021-278호)”와 “온실가스 배출권 거래제
                운영을 위한 검증 지침(환경부고시 제2021-112호)” 및 “ISO 14064-1”을 기준으로 하였습니다.`,
                                marginBottom: 20,
                            },
                            {
                                text: `라. 산정 수준`,
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `산정은 온실가스 배출권거래제 운영을 위한 검증지침에 규정된 절차에 따라 계획 및 수행되었고, 산정의 보증 수준은
                제한적 보증 수준을 만족하도록 수행되었습니다. 또한 산정 전 과정에 대한 절차가 효과적 수행되었는지 내부 심의를
                통해 확인하였습니다.`,
                                marginBottom: 20,
                            },
                            {
                                text: `마. 산정의 한계`,
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `산정은 기준 및 방법 등을 적용하는 과정에서 발생될 수 있는 고유의 한계를 내포하고 있습니다.`,
                                marginBottom: 20,
                            },
                            {
                                text: `바. 산정 결론`,
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `산정을 통해 명세서에 수록되어 있는 온실가스 배출량의 데이터에 대해 아래와 같은 결론을 제시합니다.`,
                                marginBottom: 12,
                            },
                            {
                                ol: [
                                    {
                                        text: `${date_util_1.DateUtil.toKrDateString(params.company.startDate)} ~ ${date_util_1.DateUtil.toKrDateString(params.company.endDate)} 온실가스 배출량은 “온실가스 배출권 거래제의
                    배출량 보고 및 인증에 관한 지침" 및 “ISO 14064-1” 산정 지침에 따라 적절하게 산정되었습니다.`,
                                        marginLeft: 32,
                                        marginBottom: 4,
                                    },
                                    {
                                        text: `${date_util_1.DateUtil.toKrDateString(params.company.startDate)} ~ ${date_util_1.DateUtil.toKrDateString(params.company.endDate)} 온실가스 배출량 산정에 사용된 데이터와 정보는
                    적절하고 합리적이며, 검증 의견에 영향을 줄 수 있는 중대한 오류 및 누락은 발견되지 않았습니다.`,
                                        marginLeft: 32,
                                        marginBottom: 4,
                                    },
                                    {
                                        text: `따라서, ${params.company.name}의 ${date_util_1.DateUtil.toKrDateString(params.company.startDate)} ~ ${date_util_1.DateUtil.toKrDateString(params.company.endDate)} 온실가스 배출량에 대한
                    ${params.result} 의견을 제시합니다.`,
                                        marginLeft: 32,
                                        marginBottom: 28,
                                    },
                                ],
                            },
                            {
                                alignment: 'right',
                                stack: [
                                    {
                                        text: date_util_1.DateUtil.toKrDateString(params.publishedAt),
                                        bold: true,
                                    },
                                    {
                                        text: `(주)리뉴어스랩 대표 이재용`,
                                        fontSize: 14,
                                        bold: true,
                                    },
                                ],
                            },
                        ],
                    },
                    // 부록A
                    // Header
                    {
                        pageBreak: 'before',
                        alignment: 'justify',
                        stack: [
                            {
                                columns: [
                                    {
                                        image: 'logo',
                                        width: 100,
                                        height: 28,
                                        marginLeft: 16,
                                    },
                                    {
                                        alignment: 'center',
                                        marginTop: 8,
                                        width: '58%',
                                        bold: true,
                                        fontSize: 14,
                                        text: '부록A. 배출량 산정 결과',
                                    },
                                    {
                                        style: { color: '#575757' },
                                        fontSize: 8,
                                        stack: [
                                            {
                                                text: `문서번호 : ${params.docId}`,
                                            },
                                            {
                                                text: `발송일자 : ${date_util_1.DateUtil.toKrDateString(params.publishedAt)}`,
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0,
                                        y1: 8,
                                        x2: 520,
                                        y2: 8,
                                        lineWidth: 1.5,
                                        lineColor: '#575757',
                                    },
                                ],
                            },
                        ],
                    },
                    // Main
                    {
                        marginTop: 28,
                        stack: [
                            {
                                text: '가. 배출량 산정 조직명',
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `${params.company.name}`,
                                marginBottom: 28,
                            },
                            {
                                text: '나. 산정 대상 기간',
                                style: { fontSize: 12, bold: true },
                            },
                            {
                                text: `${params.company.name}의 배출량 데이터 산정 기간은 ${date_util_1.DateUtil.toKrDateString(params.company.startDate)} ~ ${date_util_1.DateUtil.toKrDateString(params.company.endDate)}까지 입니다.`,
                                marginBottom: 28,
                            },
                            {
                                columns: [
                                    {
                                        text: '다. 배출량 산정 결과',
                                        style: { fontSize: 12, bold: true },
                                    },
                                    {
                                        text: '(tCO₂eq)',
                                        alignment: 'right',
                                        bold: true,
                                    },
                                ],
                            },
                            {
                                alignment: 'center',
                                table: {
                                    widths: [100, 66, 100, 100, 100],
                                    body: [
                                        ['법인명', '연도', 'Scope 1', 'Scope 2', '총계'].map((text) => ({
                                            color: '#ffffff',
                                            fillColor: '#878787',
                                            border: [0, 0, 0, 0],
                                            margin: [4, 8, 4, 4],
                                            alignment: 'center',
                                            fontSize: 12,
                                            bold: true,
                                            text,
                                        })),
                                        [
                                            {
                                                text: params.company.name,
                                            },
                                            {
                                                text: params.carbonEmissionData
                                                    .map((each) => each.baseYear || '-')
                                                    .join('\n'),
                                            },
                                            {
                                                text: params.carbonEmissionData
                                                    .map((each) => each.scope1 || '-')
                                                    .join('\n'),
                                            },
                                            {
                                                text: params.carbonEmissionData
                                                    .map((each) => each.scope2 || '-')
                                                    .join('\n'),
                                            },
                                            {
                                                text: params.carbonEmissionData
                                                    .map((each) => each.scope1 + each.scope2 || '-')
                                                    .join('\n'),
                                            },
                                        ].map((text) => ({
                                            marginTop: 20,
                                            border: [0, 0, 0, 1],
                                            lineHeight: 2.8,
                                            alignment: 'center',
                                            text,
                                        })),
                                    ],
                                },
                            },
                            {
                                marginTop: 8,
                                text: '온실가스 배출량 총계는 정수 단위로 절사하며, 온실가스 산정 시트의 실제값과 ±1 tCO₂eq 미만의 차이가 발생할 수 있습니다',
                                color: '#575757',
                                fontSize: 8,
                            },
                        ],
                    },
                ],
                footer: {
                    absolutePosition: { x: 40, y: -40 },
                    style: { color: '#575757' },
                    fontSize: 8,
                    stack: [
                        {
                            columns: [
                                {
                                    marginLeft: 40,
                                    text: `발신: (주)리뉴어스랩`,
                                },
                                {
                                    marginLeft: 60,
                                    text: `수신: ${params.company.name}`,
                                },
                            ],
                        },
                        {
                            marginTop: -4,
                            canvas: [
                                {
                                    type: 'line',
                                    x1: 0,
                                    y1: 8,
                                    x2: 520,
                                    y2: 8,
                                    lineWidth: 1,
                                    lineColor: '#575757',
                                },
                            ],
                        },
                        {
                            marginTop: 8,
                            columns: [
                                {
                                    marginLeft: 40,
                                    stack: [
                                        {
                                            text: '(주)리뉴어스랩 대표 : 이재용',
                                        },
                                        {
                                            text: 'E-mail : ceo@renewearth-lab.io',
                                        },
                                    ],
                                },
                                {
                                    marginLeft: 60,
                                    stack: [
                                        {
                                            text: `명세서 수신자 : ${params.user.name}`,
                                        },
                                        {
                                            text: `E-mail : ${params.user.email}`,
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
                defaultStyle: {
                    font: 'Spoqa',
                    fontSize: 10,
                    color: '#353535',
                    lineHeight: 1.4,
                },
                images: {
                    logo: 'static/images/logo.png',
                    watermark: 'static/images/watermark.png',
                },
                background: () => ({
                    image: 'watermark',
                    alignment: 'center',
                    width: 256,
                    absolutePosition: { y: 216 },
                }),
            });
        }
        if (!pdfDoc) {
            throw new Error(`invalid pdf type. ${type}`);
        }
        pdfDoc.end();
        return pdfDoc;
    }
};
exports.PdfManager = PdfManager;
exports.PdfManager = PdfManager = __decorate([
    (0, common_1.Injectable)()
], PdfManager);
//# sourceMappingURL=pdf.manager.js.map