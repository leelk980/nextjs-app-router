"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtManager = void 0;
const common_1 = require("../../common");
const common_2 = require("@nestjs/common");
const jsonwebtoken_1 = require("jsonwebtoken");
let JwtManager = class JwtManager {
    encode(params) {
        switch (params.type) {
            case 'access':
                return (0, jsonwebtoken_1.sign)(params, common_1.environment.jwt.access.secret, {
                    expiresIn: common_1.environment.jwt.access.expire,
                });
            case 'refresh':
                return (0, jsonwebtoken_1.sign)(params, common_1.environment.jwt.refresh.secret, {
                    expiresIn: common_1.environment.jwt.refresh.expire,
                });
            default:
                throw new Error(`not implemented jwt type. (encode)`);
        }
    }
    verify(params) {
        try {
            const payload = (() => {
                switch (params.type) {
                    case 'access':
                        return (0, jsonwebtoken_1.verify)(params.token, common_1.environment.jwt.access.secret);
                    case 'refresh':
                        return (0, jsonwebtoken_1.verify)(params.token, common_1.environment.jwt.refresh.secret);
                    default:
                        throw new Error(`not implemented jwt type. (verify)`);
                }
            })();
            return payload;
        }
        catch (err) {
            return null;
        }
    }
};
exports.JwtManager = JwtManager;
exports.JwtManager = JwtManager = __decorate([
    (0, common_2.Injectable)()
], JwtManager);
//# sourceMappingURL=jwt.manager.js.map