/// <reference types="pdfkit" />
type PdfTypeEnum = {
    companySpecification: {
        docId: string;
        publishedAt: Date;
        result: '미흡' | '다소 미흡' | '적정' | '우수' | '매우 우수';
        company: {
            name: string;
            companyFacilities: {
                name: string;
                address: string;
            }[];
            startDate: Date;
            endDate: Date;
        };
        user: {
            name: string;
            email: string;
        };
        carbonEmissionData: {
            baseYear: string;
            scope1: number;
            scope2: number;
        }[];
    };
};
export declare class PdfManager {
    private printer;
    generatePdfStream<T extends keyof PdfTypeEnum>(type: T, params: PdfTypeEnum[T]): PDFKit.PDFDocument;
}
export {};
