"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorageManager = void 0;
const s3_request_presigner_1 = require("@aws-sdk/s3-request-presigner");
const common_1 = require("../../common");
const common_2 = require("@nestjs/common");
const crypto_1 = require("crypto");
const stream_1 = require("stream");
const client_s3_1 = require("@aws-sdk/client-s3");
let StorageManager = class StorageManager {
    constructor() {
        this.bucket = common_1.environment.aws.s3.bucket;
        this.s3Client = new client_s3_1.S3Client({
            region: 'ap-northeast-2',
            credentials: {
                accessKeyId: common_1.environment.aws.access,
                secretAccessKey: common_1.environment.aws.secret,
            },
        });
    }
    async generatePresignedUrl(params) {
        const { aggregate, aggregateId, entityId, type } = params;
        // TODO: date 정보 추가
        const command = new client_s3_1.PutObjectCommand({
            Bucket: this.bucket,
            Key: `${common_1.environment.node.env}/temp/${aggregate}/${aggregateId}/${type}:${entityId}:${(0, crypto_1.randomUUID)()}`,
        });
        return decodeURIComponent(await (0, s3_request_presigner_1.getSignedUrl)(this.s3Client, command, { expiresIn: 60 }));
    }
    async getPrivateSignedUrl(path) {
        const command = new client_s3_1.GetObjectCommand({
            Bucket: this.bucket,
            Key: path,
        });
        return decodeURIComponent(await (0, s3_request_presigner_1.getSignedUrl)(this.s3Client, command, { expiresIn: 60 }));
    }
    async getStorageObjectByPath(path) {
        var _a;
        const elements = (_a = path
            .split(common_1.environment.node.env)[1]) === null || _a === void 0 ? void 0 : _a.split('/').reduce((acc, each, index) => {
            if (index <= 1) {
                // 0 === '' && 1 === 'temp' or 'public'
                return acc;
            }
            else if (index === 4) {
                // 4 === "type:entityId:key"
                acc.push(...each.split(':'));
            }
            else {
                acc.push(each);
            }
            return acc;
        }, []);
        if (!elements || elements.length !== 5) {
            return null;
        }
        const object = await this.s3Client
            .send(new client_s3_1.GetObjectCommand({
            Bucket: this.bucket,
            Key: path,
        }))
            .catch(() => null);
        if (!object) {
            return null;
        }
        return {
            aggregate: elements[0],
            aggregateId: +elements[1],
            type: elements[2],
            entityId: +elements[3],
            key: elements[4],
            getBuffer: () => this.streamToBuffer(object.Body),
        };
    }
    streamToBuffer(stream) {
        return new Promise((resolve, reject) => {
            const chunks = [];
            stream.on('data', (chunk) => chunks.push(chunk));
            stream.on('error', reject);
            stream.on('end', () => resolve(Buffer.concat(chunks)));
        });
    }
    async moveStorageTempObject(tempPath) {
        try {
            if (!tempPath.includes('/temp/')) {
                return 'invalid temp path.';
            }
            await this.s3Client.send(new client_s3_1.CopyObjectCommand({
                Bucket: this.bucket,
                Key: tempPath.replace('temp', 'public'),
                CopySource: `${this.bucket}/${tempPath}`,
            }));
            await this.s3Client.send(new client_s3_1.DeleteObjectCommand({
                Bucket: this.bucket,
                Key: tempPath,
            }));
            return null;
        }
        catch (err) {
            return err.message;
        }
    }
    async putStorageObject(params) {
        const key = `${common_1.environment.node.env}${params.pathWithoutEnv}`;
        try {
            await this.s3Client.send(new client_s3_1.PutObjectCommand({
                Bucket: this.bucket,
                Key: key,
                ContentType: params.contentType,
                Body: params.body instanceof stream_1.Stream ? await this.streamToBuffer(params.body) : params.body,
            }));
            return { key, error: null };
        }
        catch (err) {
            return {
                key,
                error: (err.message || err),
            };
        }
    }
};
exports.StorageManager = StorageManager;
exports.StorageManager = StorageManager = __decorate([
    (0, common_2.Injectable)()
], StorageManager);
//# sourceMappingURL=storage.manager.js.map