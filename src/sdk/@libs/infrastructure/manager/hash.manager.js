"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HashManager = void 0;
const common_1 = require("@nestjs/common");
const bcryptjs_1 = require("bcryptjs");
let HashManager = class HashManager {
    async generateHash(text, rounds = 12) {
        const salt = await new Promise((resolve, reject) => {
            (0, bcryptjs_1.genSalt)(rounds, function (err, salt) {
                if (err)
                    reject(err);
                resolve(salt);
            });
        });
        return new Promise((resolve, reject) => {
            (0, bcryptjs_1.hash)(text, salt, function (err, hashed) {
                if (err)
                    reject(err);
                resolve(hashed);
            });
        });
    }
    async validateHash(text, hash) {
        return new Promise((resolve, reject) => {
            (0, bcryptjs_1.compare)(text, hash, function (err, result) {
                if (err)
                    reject(err);
                resolve(result);
            });
        });
    }
};
exports.HashManager = HashManager;
exports.HashManager = HashManager = __decorate([
    (0, common_1.Injectable)()
], HashManager);
//# sourceMappingURL=hash.manager.js.map