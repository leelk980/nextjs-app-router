import { UserRoleEnum } from '@libs/common';
type JsonWebToken = {
    type: 'access';
    userId: number;
    companyId: number;
    userRoles: UserRoleEnum[];
} | {
    type: 'refresh';
    userId: number;
    test: string;
};
export declare class JwtManager {
    encode(params: JsonWebToken): string;
    verify<T extends JsonWebToken['type'], U extends Extract<JsonWebToken, {
        type: T;
    }>>(params: {
        type: T;
        token: string;
    }): U | null;
}
export {};
