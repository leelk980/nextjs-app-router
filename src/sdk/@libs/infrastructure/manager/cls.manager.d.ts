import { Prisma } from '@prisma/client';
import { ClsService } from 'nestjs-cls';
type AsyncStorageItem = {
    trx: Prisma.TransactionClient;
    currUser: {
        id: number;
        companyId: number;
    };
};
export declare class ClsManager {
    private readonly clsService;
    constructor(clsService: ClsService);
    getItem<K extends keyof AsyncStorageItem, V extends AsyncStorageItem[K]>(key: K): V;
    setItem<K extends keyof AsyncStorageItem, V extends AsyncStorageItem[K]>(key: K, value: V): void;
}
export {};
