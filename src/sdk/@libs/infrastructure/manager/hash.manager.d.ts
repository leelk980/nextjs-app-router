export declare class HashManager {
    generateHash(text: string, rounds?: number): Promise<string>;
    validateHash(text: string, hash: string): Promise<boolean>;
}
