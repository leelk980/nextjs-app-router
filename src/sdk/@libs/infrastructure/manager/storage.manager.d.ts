/// <reference types="node" />
/// <reference types="node" />
import { StorageObjectEnum } from '@libs/common';
import { Stream } from 'stream';
export declare class StorageManager {
    private readonly bucket;
    private readonly s3Client;
    generatePresignedUrl(params: {
        aggregate: 'user' | 'company';
        aggregateId: number;
        type: StorageObjectEnum;
        entityId: number;
    }): Promise<string>;
    getPrivateSignedUrl(path: string): Promise<string>;
    getStorageObjectByPath(path: string): Promise<{
        aggregate: "company" | "user";
        aggregateId: number;
        type: "company_facilty_energyUsage_data";
        entityId: number;
        key: string;
        getBuffer: () => Promise<Buffer>;
    } | null>;
    private streamToBuffer;
    moveStorageTempObject(tempPath: string): Promise<string | null>;
    putStorageObject(params: {
        pathWithoutEnv: string;
        body: Stream | Buffer;
        contentType: 'application/pdf';
    }): Promise<{
        key: string;
        error: null;
    } | {
        key: string;
        error: string;
    }>;
}
