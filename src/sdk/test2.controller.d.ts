import { AppException } from '@libs/common';
export declare class Test2Controller {
    tvKoreanRank(): Promise<TvProgram[]>;
    tvAmericanRank(): Promise<TvProgram[]>;
    tvUpcommingRelease(): Promise<TvProgram[]>;
}
export interface TvProgram {
    id: number;
    rank: number;
    title: string;
    year: string;
    country: string;
    thumbnailUrl: string;
}
export declare class TvKoreanRankException extends AppException<'notfound tvKoreanRank.'> {
}
export declare class TvAmericanRankException extends AppException<'notfound tvAmericanRank.'> {
}
export declare class TvUpcommingReleaseException extends AppException<'notfound tvUpcommingRelease.'> {
}
