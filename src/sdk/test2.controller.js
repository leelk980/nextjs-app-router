"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TvUpcommingReleaseException = exports.TvAmericanRankException = exports.TvKoreanRankException = exports.Test2Controller = void 0;
const common_1 = require("./@libs/common");
const auth_guard_1 = require("./@libs/middleware/auth.guard");
const core_1 = require("@nestia/core");
const common_2 = require("@nestjs/common");
let Test2Controller = class Test2Controller {
    async tvKoreanRank() {
        return new Array(10).fill(0).map(() => ({
            id: 4,
            rank: 1,
            title: '살인자ㅇ난감',
            year: '2024',
            country: '한국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/JiqYuHz-6lQfxHVspxm7Bw.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFME5qZzNOelU0TVRVMk1qRXpNemtpZlEuOTNSVlVnLWJwSjQxRGNrSVFzbU5qNDFIQU0yMVpWaF9JZTVxZTFMdnFNcw',
        }));
    }
    async tvAmericanRank() {
        return new Array(10).fill(0).map(() => ({
            id: 5,
            rank: 1,
            title: '더 보이즈 시즌 1',
            year: '2019',
            country: '미국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/imbjY58XvgXekBXf5t69ww.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXhMM1Y1YTNSck5XeG9aWHBxZUdWb2JIVmhkMjl6SW4wLlhCcHg0Z3U4RTF2WFpycWZtOFJ3RllzaXNjRUZvOTNPRlR2U1BEc3FuYjA',
        }));
    }
    async tvUpcommingRelease() {
        return new Array(10).fill(0).map(() => ({
            id: 6,
            rank: 1,
            title: '더 뉴 룩',
            year: '2024',
            country: '미국',
            thumbnailUrl: 'https://an2-img.amz.wtchn.net/image/v2/R2MMQcqVHM1wK5TdxNBNCg.jpg?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKdmNIUnpJanBiSW1SZk5Ea3dlRGN3TUhFNE1DSmRMQ0p3SWpvaUwzWXlMM04wYjNKbEwybHRZV2RsTHpFM01EQXlNRFkwTURRMU9Ea3dNVFExTlRNaWZRLkJBUWF0SHZHcDFMY1VuVmNmNXhHTm8wNHV0UTJvcl9HeWMtWWwxYktBLUE',
        }));
    }
};
exports.Test2Controller = Test2Controller;
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "TvKoreanRankException"),
    core_1.TypedRoute.Get('/tvKoreanRank', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], Test2Controller.prototype, "tvKoreanRank", null);
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "TvAmericanRankException"),
    core_1.TypedRoute.Get('/tvAmericanRank', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], Test2Controller.prototype, "tvAmericanRank", null);
__decorate([
    (0, auth_guard_1.Auth)('public'),
    (0, core_1.TypedException)(400, undefined, "TvUpcommingReleaseException"),
    core_1.TypedRoute.Get('/tvUpcommingRelease', { type: "assert", assert: input => { const assert = input => {
            const __is = input => {
                const $io0 = input => "number" === typeof input.id && !Number.isNaN(input.id) && ("number" === typeof input.rank && !Number.isNaN(input.rank)) && "string" === typeof input.title && "string" === typeof input.year && "string" === typeof input.country && "string" === typeof input.thumbnailUrl;
                return Array.isArray(input) && input.every(elem => "object" === typeof elem && null !== elem && $io0(elem));
            };
            if (false === __is(input))
                ((input, _path, _exceptionable = true) => {
                    const $guard = core_1.TypedRoute.Get.guard;
                    const $ao0 = (input, _path, _exceptionable = true) => ("number" === typeof input.id && !Number.isNaN(input.id) || $guard(_exceptionable, {
                        path: _path + ".id",
                        expected: "number",
                        value: input.id
                    })) && ("number" === typeof input.rank && !Number.isNaN(input.rank) || $guard(_exceptionable, {
                        path: _path + ".rank",
                        expected: "number",
                        value: input.rank
                    })) && ("string" === typeof input.title || $guard(_exceptionable, {
                        path: _path + ".title",
                        expected: "string",
                        value: input.title
                    })) && ("string" === typeof input.year || $guard(_exceptionable, {
                        path: _path + ".year",
                        expected: "string",
                        value: input.year
                    })) && ("string" === typeof input.country || $guard(_exceptionable, {
                        path: _path + ".country",
                        expected: "string",
                        value: input.country
                    })) && ("string" === typeof input.thumbnailUrl || $guard(_exceptionable, {
                        path: _path + ".thumbnailUrl",
                        expected: "string",
                        value: input.thumbnailUrl
                    }));
                    return (Array.isArray(input) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    })) && input.every((elem, _index1) => ("object" === typeof elem && null !== elem || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) && $ao0(elem, _path + "[" + _index1 + "]", true) || $guard(true, {
                        path: _path + "[" + _index1 + "]",
                        expected: "TvProgram",
                        value: elem
                    })) || $guard(true, {
                        path: _path + "",
                        expected: "Array<TvProgram>",
                        value: input
                    });
                })(input, "$input", true);
            return input;
        }; const stringify = input => {
            const $string = core_1.TypedRoute.Get.string;
            return `[${input.map(elem => `{"id":${elem.id},"rank":${elem.rank},"title":${$string(elem.title)},"year":${$string(elem.year)},"country":${$string(elem.country)},"thumbnailUrl":${$string(elem.thumbnailUrl)}}`).join(",")}]`;
        }; return stringify(assert(input)); } }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], Test2Controller.prototype, "tvUpcommingRelease", null);
exports.Test2Controller = Test2Controller = __decorate([
    (0, common_2.Controller)('/tv-programs')
], Test2Controller);
class TvKoreanRankException extends common_1.AppException {
}
exports.TvKoreanRankException = TvKoreanRankException;
class TvAmericanRankException extends common_1.AppException {
}
exports.TvAmericanRankException = TvAmericanRankException;
class TvUpcommingReleaseException extends common_1.AppException {
}
exports.TvUpcommingReleaseException = TvUpcommingReleaseException;
//# sourceMappingURL=test2.controller.js.map