import { tags } from 'typia';
import { AppException } from '@libs/common';
export interface SignUpUserData {
    email: string & tags.Format<'email'>;
    password: string;
    name: string;
}
export interface SignUpUserView {
    id: number;
}
export declare class SignUpUserException extends AppException<'already email exist.'> {
}
