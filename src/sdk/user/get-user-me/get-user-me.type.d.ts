import { AppException } from '@libs/common';
export interface GetUserMeView {
    id: number;
    email: string;
    name: string;
}
export declare class GetUserMeException extends AppException<'user not found.'> {
}
