export const debounce = <T extends (...params: any[]) => void>(fn: T, delay: number) => {
  let lastExecutionTime = 0;

  return (...args: Parameters<T>): void => {
    const currentTime = Date.now();

    if (currentTime - lastExecutionTime >= delay) {
      lastExecutionTime = currentTime;
      fn(...args);
    }
  };
};

export const debounceLast = <T extends (...params: any[]) => void>(fn: T, delay: number) => {
  let timeoutId: NodeJS.Timeout | null = null;

  return (...args: Parameters<T>): void => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    timeoutId = setTimeout(() => {
      timeoutId = null;
      fn(...args);
    }, delay);
  };
};

export const throttle = <T extends (...params: any[]) => void>(fn: T, delay: number) => {
  let lastReservationTime = 0;
  let lastExecutionTime = 0;

  return (...args: Parameters<T>): void => {
    const currentTime = Date.now();

    if (currentTime - lastExecutionTime >= delay) {
      lastReservationTime = currentTime;
      lastExecutionTime = currentTime;
      fn(...args);
    } else {
      lastReservationTime += delay;

      setTimeout(() => {
        lastExecutionTime = Date.now();
        fn(...args);
      }, lastReservationTime - currentTime);
    }
  };
};

export const sleep = (second: number) => new Promise((resolve) => setTimeout(resolve, second));

export * from './validator';
