class Validator {
  checkEmail(text: string) {
    return text.includes('@') ? null : ('잘못된 이메일 양식입니다.' as const);
  }

  checkPassword(text: string) {
    return text.length > 3 ? null : ('비밀번호는 4자리 이상입니다.' as const);
  }
}

export const validator = new Validator();
