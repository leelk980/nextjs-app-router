import { Carousel } from '@/component/banner';

export const Banner = () => {
  return (
    <Carousel
      width={1200}
      height={200}
      images={[
        {
          src: 'https://an2-img.amz.wtchn.net/image/v2/1uQFYSjE4NgNcIXjz_qq3A.png?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKd0lqb2lMM1l5TDNOMGIzSmxMM0J5YjIxdmRHbHZiaTh5TlRNME9UZzJNamN3TVRJME5qQWlmUS50bm9VaG10WmlnWFBXUVhUWVRRY1RMWkhYWHc0ckw2cTZkbVVOTXduUnBJ',
          alt: 'image',
        },
        {
          src: 'https://an2-img.amz.wtchn.net/image/v2/DXaYsmaY-kawCiZP4BO2XQ.png?jwt=ZXlKaGJHY2lPaUpJVXpJMU5pSjkuZXlKd0lqb2lMM1l5TDNOMGIzSmxMM0J5YjIxdmRHbHZiaTh6T1RReU56QTFOemMxT0RneU9URWlmUS5LMlJvc0hBOWhwbVJJMnRDTHJERDhLTTQ5QnlwVmtLWC1yVzBCSVFxS0lF',
          alt: 'image',
        },
      ]}
    />
  );
};
