import { VContainer } from '@/component/container';
import { HydrationProvider } from '@/component/provider';
import { useApiPrefetchQuery } from '@/hook';

import { Banner } from './Banner';
import { BoxOfficeRank } from './BoxOfficeRank';
import { UpcommingRelease } from './UpcommingRelease';
import { WatchaRank } from './WatchaRank';

export default async function MoviePage() {
  const prefetchFns = [
    useApiPrefetchQuery((api) => api.movies.movieBoxOfficeRank).data(),
    useApiPrefetchQuery((api) => api.movies.movieUpcommingRelease).data(),
    useApiPrefetchQuery((api) => api.movies.movieWatchaRank).data(),
  ];

  return (
    <HydrationProvider fns={prefetchFns}>
      <VContainer gap={12}>
        <Banner />
        <BoxOfficeRank />
        <UpcommingRelease />
        <WatchaRank />
      </VContainer>
    </HydrationProvider>
  );
}
