'use client';
import { ContentsCard } from '@/component/card';
import { HContainer, VContainer } from '@/component/container';
import { Heading, LoadingSpinner, Text } from '@/component/ui';
import { useApiQuery } from '@/hook';

export const UpcommingRelease = () => {
  const { data, error, isLoading } = useApiQuery((api) => api.movies.movieUpcommingRelease).data();

  return (
    <VContainer alignItems={'flex-start'} spacing={4} width={'100%'}>
      <Heading size={'md'}>공개 예정작</Heading>
      <HContainer spacing={4} width={'100%'} overflowX={'scroll'}>
        {isLoading && <LoadingSpinner />}
        {error && <Text color={'gray.500'}>{error.message}</Text>}
        {data &&
          data.map((each, idx) => (
            <ContentsCard
              key={idx}
              flex={'0 0 auto'}
              movie={{
                id: each.id,
                badge: each.rank.toString(),
                title: each.title,
                year: each.year,
                country: each.country,
                thumbnailUrl: each.thumbnailUrl,
              }}
            />
          ))}
      </HContainer>
    </VContainer>
  );
};
