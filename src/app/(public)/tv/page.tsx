import { VContainer } from '@/component/container';

import { AmericanRank } from './AmerianRank';
import { KoreanRank } from './KoreanRank';
import { UpcommingRelease } from './UpcommingRelease';

export default function TvPage() {
  return (
    <VContainer gap={12}>
      <KoreanRank />
      <AmericanRank />
      <UpcommingRelease />
    </VContainer>
  );
}
