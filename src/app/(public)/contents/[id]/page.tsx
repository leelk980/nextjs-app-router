import { VContainer } from '@/component/container';
import { useApiPrefetchQuery } from '@/hook';

import { Cast } from './Cast';
import { Comment } from './Comment';
import { Description } from './Description';
import { Introduction } from './Introduction';
import { SimilarContents } from './SimilarContents';

type Props = {
  params: {
    id: string;
  };
};

export default function ConentsPage(props: Props) {
  console.log(props);

  const prefetchFns = [
    useApiPrefetchQuery((api) => api.movies.movieBoxOfficeRank).data(),
    useApiPrefetchQuery((api) => api.movies.movieUpcommingRelease).data(),
    useApiPrefetchQuery((api) => api.movies.movieWatchaRank).data(),
  ];

  return (
    <VContainer>
      <Introduction />
      <Description />
      <Cast />
      <Comment />
      <SimilarContents />
    </VContainer>
  );
}
