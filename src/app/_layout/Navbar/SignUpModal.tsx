import { ChangeEventHandler, MouseEventHandler, useState } from 'react';

import { HContainer, VContainer } from '@/component/container';
import { Modal } from '@/component/overlay';
import { Button, Heading, Input, Logo, Text } from '@/component/ui';
import { useApiMutation } from '@/hook';
import { validator } from '@/util';

type Props = {
  isOpen: boolean;
  onClose: () => void;

  onSignInModalOpen: () => void;
};

export const SignUpModal = (props: Props) => {
  const [input, setInput] = useState<{ name: string; email: string; password: string }>({
    name: '',
    email: '',
    password: '',
  });
  const [error, setError] = useState<{ email: string; password: string }>({
    email: '',
    password: '',
  });

  const postToSignUp = useApiMutation((api) => api.users.sign_up.signUpUser).data(input);

  const handleModalClose = () => {
    setInput({ name: '', email: '', password: '' });
    setError({ email: '', password: '' });

    props.onClose();
  };

  const handleInputChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    event.preventDefault();

    const { name, value } = event.target;

    setInput((prev) => ({ ...prev, [name]: value }));
  };

  const handleButtonClick: MouseEventHandler<HTMLButtonElement> = async (event) => {
    event.preventDefault();

    const emailError = validator.checkEmail(input.email) || '';
    const passwordError = validator.checkPassword(input.password) || '';
    if (emailError || passwordError) {
      return setError({ email: emailError, password: passwordError });
    }

    const { error, isPending } = await postToSignUp();
    if (isPending) {
      return;
    }
    if (error) {
      return setError({ email: '', password: error.message });
    }

    alert('회원가입 성공! 로그인하세요.');

    handleModalClose();
    handleSignInButtonClick(event);
  };

  const handleSignInButtonClick: MouseEventHandler<HTMLButtonElement> = async (event) => {
    event.preventDefault();

    handleModalClose();
    props.onSignInModalOpen();
  };

  return (
    <Modal size={'xl'} isOpen={props.isOpen} onClose={handleModalClose}>
      <VContainer spacing={8} padding={8}>
        <Logo size={'lg'} />

        <Heading size={'md'}>회원가입</Heading>

        <VContainer width={'80%'} spacing={4}>
          <Input
            name={'username'}
            placeholder={'이름'}
            autoFocus={true}
            onChange={handleInputChange}
            debounceTime={0}
          />

          <Input
            name={'email'}
            placeholder={'이메일'}
            errorMessage={error.email}
            onChange={handleInputChange}
            debounceTime={0}
          />

          <Input
            name={'password'}
            type={'password'}
            placeholder={'비밀번호'}
            errorMessage={error.password}
            onChange={handleInputChange}
            debounceTime={0}
          />

          <Button
            width={'100%'}
            colorScheme={'pink'}
            bgColor={'pink.400'}
            color={'white'}
            height={12}
            my={2}
            onClick={handleButtonClick}
          >
            회원가입
          </Button>
        </VContainer>

        <VContainer spacing={4}>
          {/* <Button variant={'link'} colorScheme={'pink'} color={'pink.400'}>
            비밀번호를 잊어버리셨나요?
          </Button> */}

          <HContainer spacing={2}>
            <Text color={'gray.400'}>이미 가입하셨나요?</Text>
            <Button
              variant={'link'}
              colorScheme={'pink'}
              color={'pink.400'}
              onClick={handleSignInButtonClick}
            >
              로그인
            </Button>
          </HContainer>
        </VContainer>
      </VContainer>
    </Modal>
  );
};
