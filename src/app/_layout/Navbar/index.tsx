'use client';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { useState } from 'react';

import { HContainer, VContainer } from '@/component/container';
import { Button, Icon, Logo, Text } from '@/component/ui';
import { useApiQuery, useGlobalStore } from '@/hook';

import { SearchModal } from './SearchModal';
import { SignInModal } from './SignInModal';
import { SignUpModal } from './SignUpModal';

type Domain = 'movie' | 'tv';

export const Navbar = () => {
  const userMe = useApiQuery((api) => api.users.me.getUserMe).data();

  const currDomain = usePathname().split('/')[1] as Domain;
  const { isDesktop } = useGlobalStore();
  const [modalOpen, setModalOpen] = useState<'search' | 'signIn' | 'signUp'>();

  const getLinkStyling = (params: Domain) => {
    if (currDomain !== params) {
      return { color: 'gray.500' };
    }

    return { as: 'b' } as const;
  };

  const closeModal = () => setModalOpen(undefined);
  const openSearchModal = () => setModalOpen('search');
  const openSignInModal = () => setModalOpen('signIn');
  const openSignUpModal = () => setModalOpen('signUp');

  return (
    <VContainer>
      {/* top */}
      <HContainer
        position={'fixed'}
        width={'100%'}
        maxWidth={1200}
        height={16}
        px={8}
        bgColor={'white'}
        justifyContent={'space-between'}
        borderBottom={'1px'}
        borderColor={'gray.200'}
        zIndex={100}
      >
        {/* top-left */}
        <HContainer spacing={6}>
          <Link href={'/'}>
            <Logo size={'md'} />
          </Link>

          <Link href={'/movie'}>
            <Text fontSize={'sm'} {...getLinkStyling('movie')}>
              영화
            </Text>
          </Link>
          <Link href={'/tv'}>
            <Text fontSize={'sm'} {...getLinkStyling('tv')}>
              TV
            </Text>
          </Link>
        </HContainer>

        {/* top-right */}
        {isDesktop && (
          <HContainer spacing={8}>
            {/* TODO: onclik event */}
            <Button onClick={openSearchModal} variant={'link'}>
              <Icon.Search color={'gray.500'} />
            </Button>

            {userMe.data && (
              <Link href={'my-page'}>
                <Button color={'gray.500'} fontSize={'sm'} variant={'outline'}>
                  내정보
                </Button>
              </Link>
            )}

            {!userMe.data && (
              <>
                <Button
                  onClick={openSignInModal}
                  color={'gray.500'}
                  fontSize={'sm'}
                  variant={'link'}
                >
                  로그인
                </Button>

                <Button onClick={openSignUpModal} fontSize={'sm'} variant={'outline'}>
                  회원가입
                </Button>
              </>
            )}
          </HContainer>
        )}
      </HContainer>

      {/* bottom */}
      {!isDesktop && (
        <HContainer
          position={'fixed'}
          left={0}
          bottom={0}
          width={'100%'}
          height={16}
          justifyContent={'space-around'}
          bgColor={'white'}
          borderTop={'1px'}
          borderColor={'gray.200'}
          zIndex={100}
        >
          <Link href={'/'}>
            <Button variant={'link'}>
              <VContainer px={4}>
                <Icon.Sun color={'gray.500'} />
                <Text color={'gray.500'} fontSize={'xs'}>
                  홈
                </Text>
              </VContainer>
            </Button>
          </Link>

          <Button onClick={openSearchModal} variant={'link'}>
            <VContainer px={4}>
              <Icon.Search color={'gray.500'} />
              <Text color={'gray.500'} fontSize={'xs'}>
                검색
              </Text>
            </VContainer>
          </Button>

          {userMe.data && (
            <Link href={'/my-page'}>
              <Button variant={'link'}>
                <VContainer px={4}>
                  <Icon.Info color={'gray.500'} />
                  <Text color={'gray.500'} fontSize={'xs'}>
                    내정보
                  </Text>
                </VContainer>
              </Button>
            </Link>
          )}

          {!userMe.data && (
            <Button onClick={openSignInModal} variant={'link'}>
              <VContainer px={4}>
                <Icon.Info color={'gray.500'} />
                <Text color={'gray.500'} fontSize={'xs'}>
                  로그인
                </Text>
              </VContainer>
            </Button>
          )}
        </HContainer>
      )}

      <SearchModal isOpen={modalOpen === 'search'} onClose={closeModal} />
      <SignInModal
        isOpen={modalOpen === 'signIn'}
        onClose={closeModal}
        onSignUpModalOpen={openSignUpModal}
      />
      <SignUpModal
        isOpen={modalOpen === 'signUp'}
        onClose={closeModal}
        onSignInModalOpen={openSignInModal}
      />
    </VContainer>
  );
};
