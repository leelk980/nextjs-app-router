import { useRouter } from 'next/navigation';
import { ChangeEventHandler, MouseEventHandler, useState } from 'react';

import { HContainer, VContainer } from '@/component/container';
import { Modal } from '@/component/overlay';
import { Button, Heading, Input, Logo, Text } from '@/component/ui';
import { useApiInvalidation, useApiMutation, useStorage } from '@/hook';
import { validator } from '@/util';

type Props = {
  isOpen: boolean;
  onClose: () => void;

  onSignUpModalOpen: () => void;
};

export const SignInModal = (props: Props) => {
  const router = useRouter();

  const [_, setAccessToken] = useStorage({ type: 'cookie', key: 'accessToken' });

  const [input, setInput] = useState<{ email: string; password: string }>({
    email: '',
    password: '',
  });
  const [error, setError] = useState<{ email: string; password: string }>({
    email: '',
    password: '',
  });

  const postToSignIn = useApiMutation((api) => api.users.sign_in.signInUser).data(input);

  const invalidateUserMe = useApiInvalidation((api) => api.users.me.getUserMe).data();

  const handleModalClose = () => {
    setInput({ email: '', password: '' });
    setError({ email: '', password: '' });

    props.onClose();
  };

  const handleInputChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    event.preventDefault();

    const { name, value } = event.target;

    setInput((prev) => ({ ...prev, [name]: value }));
  };

  const handleButtonClick: MouseEventHandler<HTMLButtonElement> = async (event) => {
    event.preventDefault();

    const emailError = validator.checkEmail(input.email) || '';
    const passwordError = validator.checkPassword(input.password) || '';
    if (emailError || passwordError) {
      return setError({ email: emailError, password: passwordError });
    }

    const { data, error, isPending } = await postToSignIn();
    if (isPending) {
      return;
    }
    if (error) {
      return setError({ email: '', password: error.message });
    }

    setAccessToken(data.accessToken);
    invalidateUserMe();
    handleModalClose();
    router.push('/');
  };

  const handleSignUpButtonClick: MouseEventHandler<HTMLButtonElement> = async (event) => {
    event.preventDefault();

    handleModalClose();
    props.onSignUpModalOpen();
  };

  return (
    <Modal size={'xl'} isOpen={props.isOpen} onClose={handleModalClose}>
      <VContainer spacing={8} padding={8}>
        <Logo size={'lg'} />

        <Heading size={'md'}>로그인</Heading>

        <VContainer width={'80%'} spacing={4}>
          <Input
            name={'email'}
            placeholder={'이메일'}
            autoFocus={true}
            errorMessage={error.email}
            onChange={handleInputChange}
            debounceTime={0}
          />

          <Input
            name={'password'}
            type={'password'}
            placeholder={'비밀번호'}
            errorMessage={error.password}
            onChange={handleInputChange}
            debounceTime={0}
          />

          <Button
            width={'100%'}
            colorScheme={'pink'}
            bgColor={'pink.400'}
            color={'white'}
            height={12}
            my={2}
            onClick={handleButtonClick}
          >
            로그인
          </Button>
        </VContainer>

        <VContainer spacing={4}>
          {/* <Button variant={'link'} colorScheme={'pink'} color={'pink.400'}>
            비밀번호를 잊어버리셨나요?
          </Button> */}

          <HContainer spacing={2}>
            <Text color={'gray.400'}>계정이 없으신가요?</Text>
            <Button
              variant={'link'}
              colorScheme={'pink'}
              color={'pink.400'}
              onClick={handleSignUpButtonClick}
            >
              회원가입
            </Button>
          </HContainer>
        </VContainer>
      </VContainer>
    </Modal>
  );
};
