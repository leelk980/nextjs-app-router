import Link from 'next/link';
import { ChangeEventHandler, useEffect, useState } from 'react';

import { VContainer } from '@/component/container';
import { Modal } from '@/component/overlay';
import { Heading, Icon, Input, Text } from '@/component/ui';
import { useApiQuery } from '@/hook';

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

export const SearchModal = (props: Props) => {
  const [searchTerm, setSearchTerm] = useState<string>('');

  const res = useApiQuery((api) => api.movies.movieWatchaRankWithQuery).data({
    searchTerm,
    id: searchTerm ? undefined : 100,
  } as any);

  const [searchResult, setSearchResult] = useState(res.data);

  useEffect(() => {
    if (!res.isLoading) setSearchResult(res.data);
  }, [res.data, res.isLoading]);

  const handleInputChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    event.preventDefault();
    setSearchTerm(event.target.value);
  };

  const handleModalClose = () => {
    setSearchTerm('');
    props.onClose();
  };

  return (
    <Modal size={'xl'} isOpen={props.isOpen} onClose={handleModalClose}>
      <VContainer alignItems={'flex-start'} spacing={8} py={8} px={8}>
        <Input
          placeholder={'콘텐츠를 검색해보세요.'}
          autoFocus={true}
          leftElement={<Icon.Search color={'gray.400'} />}
          onChange={handleInputChange}
        />

        <Heading size={'md'}>연관 검색어</Heading>

        <VContainer width={'100%'} alignItems={'flex-start'} spacing={4}>
          {searchResult?.map((each, index) => (
            <Link
              key={each.id}
              href={`/contents/${each.id}`}
              style={{ width: '100%' }}
              onClick={handleModalClose}
            >
              <Text
                fontSize={'sm'}
                pb={2}
                pl={2}
                color={'gray.500'}
                borderBottom={'1px'}
                borderColor={'gray.100'}
              >
                {`${index + 1}. ${each.title} (${each.year})`}
              </Text>
            </Link>
          ))}
        </VContainer>
      </VContainer>
    </Modal>
  );
};
