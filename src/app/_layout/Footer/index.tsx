'use client';
import Link from 'next/link';

import { HContainer, VContainer } from '@/component/container';
import { Text } from '@/component/ui';
import { useGlobalStore } from '@/hook';

export const Footer = () => {
  const { isDesktop } = useGlobalStore();

  return (
    <VContainer
      width={'100%'}
      mt={16}
      mb={isDesktop ? 0 : 16}
      px={isDesktop ? 16 : 8}
      py={isDesktop ? 12 : 8}
      bgColor={'gray.900'}
      color={'gray.400'}
      alignItems={'flex-start'}
    >
      <HContainer fontSize={'sm'}>
        <Link href={'/'}>
          <Text>서비스 이용약관</Text>
        </Link>
        <Text> | </Text>

        <Link href={'/'}>
          <Text>개인정보 처리방침</Text>
        </Link>
        <Text> | </Text>

        <Link href={'/'}>
          <Text>회사 안내</Text>
        </Link>
      </HContainer>

      <HContainer fontSize={'sm'}>
        <Text>고객센터 | cs@watchapedia.co.kr</Text>
      </HContainer>

      <HContainer fontSize={'sm'}>
        <Text>주식회사 왓챠 | 대표 박태훈 | 서울특별시 서초구 강남대로 343 신덕빌딩 3층</Text>
      </HContainer>
    </VContainer>
  );
};
