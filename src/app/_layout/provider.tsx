'use client';
import { useEffect, useState } from 'react';
import { CookiesProvider } from 'react-cookie';

import { useGlobalStore } from '@/hook';
import { ChakraProvider } from '@chakra-ui/react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';

export const Provider = ({ children }: { children: React.ReactNode }) => {
  const [queryClient] = useState(
    new QueryClient({
      defaultOptions: {
        queries: {
          staleTime: 10 * 1000,
          refetchOnWindowFocus: false,
        },
      },
    })
  );

  useEffect(() => {
    const handleResize = () =>
      useGlobalStore.getState().setGlobalState({ isDesktop: window.innerWidth >= 600 });

    handleResize();

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (
    <ChakraProvider>
      <CookiesProvider defaultSetOptions={{ path: '/' }}>
        <QueryClientProvider client={queryClient}>
          {children}
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </CookiesProvider>
    </ChakraProvider>
  );
};
