import { HContainer } from '@/component/container';
import { Heading } from '@/component/ui';

export default function NotfoundPage() {
  return (
    <HContainer width={'100%'} height={'80vh'} justifyContent={'center'}>
      <Heading size={'md'}>not found | 404</Heading>
    </HContainer>
  );
}
