'use client';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';

import { HContainer, VContainer } from '@/component/container';
import { Modal } from '@/component/overlay';
import { Button, Heading, Text } from '@/component/ui';
import { useApiInvalidation, useStorage } from '@/hook';

export const Logout = () => {
  const router = useRouter();

  const [modalOpen, setModalOpen] = useState(false);

  const [, , removeAccessToken] = useStorage({ type: 'cookie', key: 'accessToken' });

  const invalidateUserMe = useApiInvalidation((api) => api.users.me.getUserMe).data();

  const handleButtonClick = () => {
    removeAccessToken();
    invalidateUserMe();
    router.push('/');
  };

  useEffect(() => {}, []);
  return (
    <VContainer>
      <Button onClick={() => setModalOpen(true)}>로그아웃</Button>

      <Modal size={'sm'} isOpen={modalOpen} onClose={() => setModalOpen(false)}>
        <VContainer py={8} px={4} spacing={8}>
          <Heading size={'md'}>알림</Heading>
          <Text fontSize={'lg'}>로그아웃 하시겠어요?</Text>
          <HContainer>
            <Button width={32} onClick={handleButtonClick}>
              네
            </Button>
            <Button width={32} onClick={() => setModalOpen(false)}>
              아니요
            </Button>
          </HContainer>
        </VContainer>
      </Modal>
    </VContainer>
  );
};
