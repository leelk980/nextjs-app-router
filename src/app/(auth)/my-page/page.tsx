import { VContainer } from '@/component/container';

import { Logout } from './Logout';

export default function MyPagePage() {
  return (
    <VContainer>
      <h2 style={{ fontSize: '32px', marginBottom: '32px' }}>마이페이지</h2>
      <Logout />
    </VContainer>
  );
}
