'use client';
import { useRouter } from 'next/navigation';
import { useEffect } from 'react';

import { useApiQuery } from '@/hook';

export default function RootLayout({ children }: { children: React.ReactNode }) {
  const router = useRouter();
  const userMe = useApiQuery((api) => api.users.me.getUserMe).data();

  useEffect(() => {
    if (userMe.error) {
      router.push('404');
    }
  }, [router, userMe.error]);

  return <>{children}</>;
}
