import { Metadata } from 'next';
import { Inter } from 'next/font/google';

import { HydrationProvider } from '@/component/provider';
import { useApiPrefetchQuery } from '@/hook';

import { Footer } from './_layout/Footer';
import { Navbar } from './_layout/Navbar';
import { Provider } from './_layout/provider';

const inter = Inter({ subsets: ['latin'] });

export const revalidate = 0;

export const metadata: Metadata = {
  title: 'nextjs demo',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  const fns = [useApiPrefetchQuery((api) => api.users.me.getUserMe).data()];

  return (
    <html lang="en">
      <body className={inter.className}>
        <Provider>
          <HydrationProvider fns={fns}>
            <Navbar />
            <div
              style={{
                width: '100%',
                maxWidth: '1200px',
                margin: 'auto',
                padding: '84px calc(0.05 * 100vw) 0px calc(0.05 * 100vw)',
              }}
            >
              {children}
            </div>
            <Footer />
          </HydrationProvider>
        </Provider>
      </body>
    </html>
  );
}
